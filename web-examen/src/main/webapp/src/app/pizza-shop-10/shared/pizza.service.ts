import {Injectable} from '@angular/core';
import {Http, Response, Headers} from "@angular/http";

import {Pizza} from "./pizza.model";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class PizzaService {
  private pizzasUrl = 'http://localhost:8080/api/pizzas';
  private pageUrl = 'http://localhost:8080/api/page/';
  private filtersUrl = 'http://localhost:8080/api/pizzas/filter';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  getPizzas(): Observable<Pizza[]> {
    return this.http.get(this.pizzasUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }


  getPage(page:number): Observable<Pizza[]> {
    const url = `${this.pageUrl}/${page}`
    return this.http.get(url)
      .map(this.extractData)
      .catch(this.handleError);
  }

  filter(body: Object){
    return this.http.post(this.filtersUrl,JSON.stringify(body),{headers: this.headers})
      .map(this.extractData)
      .catch(this.handleError);

  }

  private extractData(res: Response) {
    let body = res.json();
    return body.pizzas || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getPizza(id: number): Observable<Pizza> {
    return this.getPizzas()
      .map(pizzas => pizzas.find(pizza => pizza.id === id));
  }

  update(pizza): Observable<Pizza> {
    const url = `${this.pizzasUrl}/${pizza.id}`;
    return this.http
      .put(url, JSON.stringify({"pizza": pizza}), {headers: this.headers})
      .map(this.extractStudentData)
      .catch(this.handleError);
  }

  private extractStudentData(res: Response) {
    let body = res.json();
    return body.pizza || {};
  }

  create(name: string, description: string, price: number): Observable<Pizza> {
    let pizza = {name, description, price,};
    return this.http
      .post(this.pizzasUrl, JSON.stringify({"pizza": pizza}), {headers: this.headers})
      .map(this.extractStudentData)
      .catch(this.handleError);
  }

  delete(id: number): Observable<void> {
    const url = `${this.pizzasUrl}/${id}`;
    return this.http
      .delete(url, {headers: this.headers})
      .map(() => null)
      .catch(this.handleError);
  }
}
