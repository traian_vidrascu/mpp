import { Component, OnInit } from '@angular/core';
import {PizzaService} from "../shared/pizza.service";
import {Pizza} from "../shared/pizza.model";

@Component({
  selector: 'app-pizza-shop-10-filter',
  templateUrl: './pizza-shop-10-filter.component.html',
  styleUrls: ['./pizza-shop-10-filter.component.css']
})
export class PizzaShop10FilterComponent implements OnInit {

  isCusine: boolean = false;
  lessThan: boolean = false;
  cusine: string = null;
  price: number = null;
  pizzas: Pizza[];
  cusines = ['MEDITERRANEAN','ORIENTAL'];

  constructor(private pizzaService: PizzaService) { }

  ngOnInit() {
    this.pizzaService.getPizzas().subscribe(
      pizzas => this.pizzas = pizzas
    )
  }

  filter(){
    let body = {filters: []};
    if(this.isCusine == true){

      if(this.cusine == null){

        alert("Cusine is null");
        return;

      }
      body.filters.push({
        filterType: "isCusine",
        filterValue: this.cusine
      })
    }

    if(this.lessThan == true){

      if(this.price == null){

        alert("price is null");
        return;
      }
      body.filters.push({
        filterType: "lessThan",
        filterValue: this.price
      })
    }

    if(this.lessThan == false && this.isCusine == false){

      this.pizzaService.getPizzas().subscribe(
        pizzas => this.pizzas = pizzas
      )
    }
    else {
      this.pizzaService.filter(body).subscribe(
        pizzas => this.pizzas = pizzas
      )
    }
  }
}
