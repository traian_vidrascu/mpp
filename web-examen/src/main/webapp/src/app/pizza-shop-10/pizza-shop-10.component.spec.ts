import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaShop10Component } from './pizza-shop-10.component';

describe('PizzaShop10Component', () => {
  let component: PizzaShop10Component;
  let fixture: ComponentFixture<PizzaShop10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PizzaShop10Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaShop10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
