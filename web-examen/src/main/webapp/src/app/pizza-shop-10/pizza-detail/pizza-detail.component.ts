import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {IngredientService} from "../shared/ingredient.service";
import {ActivatedRoute, Params} from "@angular/router";
import {PizzaService} from "../shared/pizza.service";
import {Ingredient} from "../shared/ingredient.model";

@Component({
  selector: 'app-pizza-detail',
  templateUrl: './pizza-detail.component.html',
  styleUrls: ['./pizza-detail.component.css']
})
export class PizzaDetailComponent implements OnInit {

  ingredients: Ingredient[];

  constructor(private ingredientService: IngredientService,
              private pizzaService: PizzaService,
              private route: ActivatedRoute,
              private location: Location) { }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.ingredientService.getIngredients(+params['id']))
      .subscribe(ingredients => this.ingredients = ingredients);
  }

  goBack(): void {
    this.location.back();
  }

}
