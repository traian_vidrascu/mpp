import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { PizzaService } from './pizza-shop-10/shared/pizza.service';
import { PizzaShop10Component } from './pizza-shop-10/pizza-shop-10.component';
import { PizzaShop10NewComponent } from './pizza-shop-10/pizza-shop-10-new/pizza-shop-10-new.component';
import { PizzaShop10FilterComponent } from './pizza-shop-10/pizza-shop-10-filter/pizza-shop-10-filter.component';
import { PizzaPaginationComponent } from './pizza-shop-10/pizza-pagination/pizza-pagination.component';
import { PizzaDetailComponent } from './pizza-shop-10/pizza-detail/pizza-detail.component';
import {IngredientService} from "./pizza-shop-10/shared/ingredient.service";

@NgModule({
  declarations: [
    AppComponent,
    PizzaShop10Component,
    PizzaShop10NewComponent,
    PizzaShop10FilterComponent,
    PizzaPaginationComponent,
    PizzaDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path:'pizzashop10',
        component:PizzaShop10Component
      },
      {
        path:'pizzashop10/new',
        component:PizzaShop10NewComponent
      },
      {
        path:'pizzashop10/filter',
        component:PizzaShop10FilterComponent
      },
      {
        path:'pizzashop10/pizzas',
        component:PizzaPaginationComponent
      },
      {
        path:'pizzashop10/detail/:id',
        component:PizzaDetailComponent
      }
    ])
  ],
  providers: [PizzaService,IngredientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
