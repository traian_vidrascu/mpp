package traian.examen.web.converter;

import org.springframework.stereotype.Component;
import traian.examen.core.model.Pizza;
import traian.examen.web.dto.PizzaDto;

/**
 * Created by traian on 18.06.2017.
 */
@Component
public class PizzaConverter extends BaseConverter<Pizza,PizzaDto> {

    @Override
    public Pizza convertDtoToModel(PizzaDto pizzaDto){

        Pizza pizza = Pizza.builder()
                            .name(pizzaDto.getName())
                            .description(pizzaDto.getDescription())
                            .price(pizzaDto.getPrice())
                            .build();

        pizza.setId(pizzaDto.getId());

        return pizza;
    }

    @Override
    public PizzaDto convertModelToDto(Pizza pizza){

        PizzaDto pizzaDto = PizzaDto.builder()
                                    .name(pizza.getName())
                                    .description(pizza.getDescription())
                                    .price(pizza.getPrice())
                                    .build();

        pizzaDto.setId(pizza.getId());

        return pizzaDto;
    }
}
