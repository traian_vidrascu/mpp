package traian.examen.web.converter;

import traian.examen.core.model.BaseEntity;
import traian.examen.web.dto.BaseDto;

/**
 * Created by traian on 16.06.2017.
 */
public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto>
        extends ConverterGeneric<Model, Dto> {

}