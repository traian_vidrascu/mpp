package traian.examen.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by traian on 18.06.2017.
 */
@JsonSerialize
public class EmptyJsonResponse {
}
