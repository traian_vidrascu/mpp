package traian.examen.web.dto;

import lombok.*;
import traian.examen.core.model.Ingredient;

import java.util.List;

/**
 * Created by traian on 19.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
public class IngredientsDto {
    List<IngredientDto> ingredients;
}
