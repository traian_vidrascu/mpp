package traian.examen.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import traian.examen.core.model.Cusine;
import traian.examen.core.model.Ingredient;
import traian.examen.core.model.Pizza;
import traian.examen.core.service.PizzaService;
import traian.examen.web.converter.IngredientConverter;
import traian.examen.web.converter.PizzaConverter;
import traian.examen.web.dto.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by traian on 18.06.2017.
 */
@RestController
public class PizzaController {

    private static final Logger log = LoggerFactory.getLogger(PizzaController.class);

    @Autowired
    private PizzaConverter pizzaConverter;

    @Autowired
    private IngredientConverter ingredientConverter;

    @Autowired
    private PizzaService pizzaService;

    @RequestMapping(value = "/pizzas",method = RequestMethod.GET)
    public PizzasDto getPizzas(){

        log.trace("getPizzas: entered");

        List<Pizza> pizzas = pizzaService.findAll();

        log.trace("getPizzas: pizzas={}",pizzas);

        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));
    }

    @RequestMapping(value = "/pizzas",method = RequestMethod.POST)
    public Map<String,PizzaDto> createPizza(@RequestBody final Map<String,PizzaDto> pizzaDtoMap){

        log.trace("createPizza: pizzaDtoMap={}",pizzaDtoMap);

        PizzaDto pizzaDto = pizzaDtoMap.get("pizza");

        Pizza pizza = pizzaService.create(pizzaDto.getName(),pizzaDto.getDescription(),pizzaDto.getPrice());

        Map<String,PizzaDto> result = new HashMap<>();

        result.put("pizza",pizzaConverter.convertModelToDto(pizza));

        log.trace("createPizza: result={}",result);

        return result;
    }

    @RequestMapping(value = "/pizzas/filter",method = RequestMethod.POST)
    public PizzasDto filterPizzas(@RequestBody final FiltersDto filtersDto){

        log.trace("filterPizzas: filtersDto={}",filtersDto);

        Predicate<Pizza> predicate = new Predicate<Pizza>() {
            @Override
            public boolean test(Pizza pizza) {
                Optional<FilterDto> lessThan = filtersDto.getFilters()
                                                    .stream()
                                                    .filter(f -> f.getFilterType().equals("lessThan"))
                                                    .findFirst();

                if(lessThan.isPresent()){

                    Float value = Float.valueOf(lessThan.get().getFilterValue());

                    log.trace("lessThan {}",value);

                    if(pizza.getPrice() > value){
                        return false;
                    }
                }

                Optional<FilterDto> filterCusine = filtersDto.getFilters()
                                                        .stream()
                                                        .filter(f -> f.getFilterType().equals("isCusine"))
                                                        .findFirst();

                return true;
            }
        };

        List<Pizza> pizzas = pizzaService.filter(predicate);

        log.trace("filterPizzas: pizzas={}",pizzas);

        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));
    }

    @RequestMapping(value = "/page/{page}",method = RequestMethod.GET)
    public PizzasDto getPage(@PathVariable final Integer page){

        log.trace("getPage: page={}",page);

        List<Pizza> pizzas = pizzaService.pagiantion(page);

        log.trace("getPage: pizzas={}",pizzas);

        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));
    }

    @RequestMapping(value = "/pizzas/{id}",method = RequestMethod.GET)
    public IngredientsDto getIngredients(@PathVariable final long id){

        log.trace("getIngredients: id={}",id);

        List<Ingredient> ingredients = pizzaService.getIngredients(id);

        log.trace("getIngredients: ingredients={}",ingredients);

        return new IngredientsDto(ingredientConverter.convertModelsToDtos(ingredients));
    }
}
