package traian.examen.core.service;

import traian.examen.core.model.Cusine;
import traian.examen.core.model.Ingredient;
import traian.examen.core.model.Pizza;

import java.util.List;
import java.util.function.Predicate;

/**
 * Created by traian on 18.06.2017.
 */
public interface PizzaService {

    public Pizza create(String name, String description, Float price);

    public List<Pizza> findAll();

    public List<Pizza> filter(Predicate<Pizza> filter);

    public List<Pizza> pagiantion(Integer page);

    public List<Ingredient> getIngredients(Long id);
}
