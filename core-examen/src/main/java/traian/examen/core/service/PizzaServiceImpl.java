package traian.examen.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import traian.examen.core.model.Cusine;
import traian.examen.core.model.Ingredient;
import traian.examen.core.model.Pizza;
import traian.examen.core.repository.PizzaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by traian on 18.06.2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService{

    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    @Autowired
    PizzaRepository pizzaRepository;

    @Override
    @Transactional
    public Pizza create(String name, String description, Float price) {

        log.trace("create: name={},desc={},price={},cusine={}",
                    name,description,price);

        Pizza pizza = Pizza.builder()
                            .name(name)
                            .description(description)
                            .price(price)
                            //.cusine(cusine)
                            .build();

        pizza = pizzaRepository.save(pizza);

        log.trace("create: pizza={}",pizza);

        return pizza;
    }

    @Override
    public List<Pizza> findAll() {

        log.trace("findAll: entred");

        List<Pizza> pizzas = pizzaRepository.findAll();

        log.trace("findAll: pizzas={}",pizzas);

        return pizzas;
    }

    @Override
    public List<Pizza> filter(Predicate<Pizza> filter) {

        log.trace("filter: filter={}",filter);

        List<Pizza> pizzas = pizzaRepository.findAll()
                        .stream()
                        .filter(filter)
                        .collect(Collectors.toList());

        log.trace("filter: pizzas={}",pizzas);

        return pizzas;
    }

    @Override
    public List<Pizza> pagiantion(Integer page) {

        log.trace("pagiantion: page={}",page);

        PageRequest pageRequest = new PageRequest(page,2);

        List<Pizza> pagination = pizzaRepository.findAll(pageRequest).getContent();

        log.trace("pagiantion: paginatedPizzas={}",pagination);

        return pagination;
    }

    @Override
    @Transactional
    public List<Ingredient> getIngredients(Long id) {

        log.trace("getIngridients: id={}",id);

        Pizza pizza = pizzaRepository.findOne(id);

        List<Ingredient> ingredients = new ArrayList<>();

        pizza.getIngredients().forEach(ingredient -> ingredients.add(ingredient));

        log.trace("getIngridients: ingridients={}",ingredients);

        return ingredients;
    }
}
